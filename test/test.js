var http = require('http');
var assert = require('assert');
var sampleRequest = JSON.stringify(require('./example_request.json'));
var sampleResponse = JSON.stringify(require('./example_response.json'));

require('../index');

describe('Testing the sample data', function () {
    it('Test results confirms the response which have DRM enabled and have atleast one episode', function (done) {
        var postResponseHandler = function (res) {
            var responseBuffer = [];
            res.on('data', function (chunk) {
                responseBuffer.push(chunk);
            });
            res.on('end', function () {
                var responseString = responseBuffer.join('');
                assert.equal(responseString, sampleResponse);
                done();
            });
        };

        var requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': sampleRequest.length
            }
        };

        var postRequest = http.request(requestOptions, postResponseHandler);
        postRequest.write(sampleRequest);
        postRequest.end();
    });
});